
import {JsonProperty, JsonObject} from 'src/app/config/deserializer'

@JsonObject
export class Card {

  constructor(suit ,rank ,value , secondaryValue){
    this.Suit = suit
    this.Rank= rank
    this.Value= value
    this.Secondary= secondaryValue
  }
  
  @JsonProperty('Suit', String, true)
  public Suit: string = undefined;

  @JsonProperty('Rank', String, true)
  public Rank: string = undefined;

  @JsonProperty('Value', Number, true)
  public Value: Number = 0;

  @JsonProperty('Secondary', Number, true)
  public Secondary: Number = 0;
}