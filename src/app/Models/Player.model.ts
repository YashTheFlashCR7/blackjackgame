import { Card } from "./Card.model";
import {JsonProperty, JsonObject} from 'src/app/config/deserializer'

@JsonObject
export class Player {
  constructor(id ,name ,isDealer){
    this.id = id
    this.name= name
    this.isDealer= isDealer
  }

  @JsonProperty('id', Number, true)
  public id: Number = 0;

  @JsonProperty('name', String, true)
  public name: string = undefined;

  @JsonProperty('isDealer', Boolean, true)
  public isDealer: Boolean = false;
}