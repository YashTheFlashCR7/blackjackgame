import { Component, OnInit } from '@angular/core';
import { PlayerService } from 'src/app/Services/Player/player.service'
import { CardService } from 'src/app/Services/Card/card.service'
import { CommonService} from 'src/app/Services/Common/common.service'

@Component({
  selector: 'app-assessment-unit-test',
  templateUrl: './assessment-unit-test.component.html',
  styleUrls: ['./assessment-unit-test.component.css']
})
export class AssessmentUnitTestComponent implements OnInit {
PlayerModel:any[]=[]
DealerModel:any[]=[]
FinalScoreObject:any[]=[]
ShowSimulations:boolean
NameArray=['Lemmy','Andrew','Billy','Carla']
CardArray=[{cards:[{Suit:'Spades',Rank:'Ace',Value:1,Secondary:11},
                   {Suit:'Hearts',Rank:'Seven',Value:7,Secondary:7},
                   {Suit:'Diamonds',Rank:'Ace',Value:1,Secondary:11}]},

                   {cards:[{Suit:'Diamonds',Rank:'King',Value:10,Secondary:10},
                   {Suit:'Spades',Rank:'Four',Value:4,Secondary:4},
                   {Suit:'Clubs',Rank:'Four',Value:4,Secondary:4}]},

                   {cards:[{Suit:'Spades',Rank:'Two',Value:2,Secondary:2},
                   {Suit:'Damonds',Rank:'Two',Value:2,Secondary:2},
                   {Suit:'Hearts',Rank:'Two',Value:2,Secondary:2},
                   {Suit:'Diamonds',Rank:'Four',Value:4,Secondary:4},
                   {Suit:'Clubs',Rank:'Five',Value:5,Secondary:5}]},
                   
                   {cards:[{Suit:'Clubs',Rank:'Queen',Value:10,Secondary:10},
                   {Suit:'Spades',Rank:'Six',Value:6,Secondary:6},
                   {Suit:'Diamonds',Rank:'Nine',Value:9,Secondary:9}]},

                   {cards:[{Suit:'Spades',Rank:'Jack',Value:10,Secondary:10},
                   {Suit:'Hearts',Rank:'Nine',Value:9,Secondary:9},]},
                   ]


  constructor(private PlayerService:PlayerService,private CardService:CardService,private CommonService:CommonService) { }

  ngOnInit() {
    this.ShowSimulations=true
    this.createPlayerUnitObject()
    this.createDealerUnitObject()
  }

  createPlayerUnitObject(){
    for(var i=0;i<this.NameArray['length'];i++){
      this.PlayerModel.push({
        id:i+1,
        name:this.NameArray[i],
        cards:this.CardArray[i]['cards'],
        score:0,
        status:"",
        Position:'Stand'
      })
    }
    this.calculatePlayerScores()
    console.log(this.PlayerModel)
  }

  createDealerUnitObject(){
    let dealerIndex:number=this.CardArray['length'] - 1
    this.DealerModel.push({
      id:1,
      name:"Dealer",
      cards:this.CardArray[dealerIndex]['cards'],
      score:0,
      status:"",
      Position:'Stand'
    })
    this.calculateDealerScore(1)
    console.log(this.DealerModel)
  }

  calculatePlayerScores() {

    for (var j = 0; j < this.PlayerModel['length']; j++) {
        this.PlayerModel[j]['score'] = this.CommonService.calculateCardValue(this.PlayerModel[j]['cards'])
        this.PlayerModel[j]['status'] = this.CommonService.checkIfBust(this.PlayerModel[j]['score'])
    }

}

calculateDealerScore(id) {
  id = id - 1
  this.DealerModel[id]['score'] = this.CommonService.calculateCardValue(this.DealerModel[id]['cards'])
  this.DealerModel[0]['status'] = this.CommonService.checkIfBust(this.DealerModel[0]['score'])
}

CheckEndGameScenarios() {
  for (var i = 1; i < this.PlayerModel['length'] + 1; i++) {
      this.FinalScoreObject.push(this.CommonService.CheckPlayerWinStatus(this.PlayerModel, i, this.DealerModel))
  }
  console.log(this.FinalScoreObject)
  this.ShowSimulations=false
}

}
