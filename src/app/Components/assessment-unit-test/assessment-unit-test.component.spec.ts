import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssessmentUnitTestComponent } from './assessment-unit-test.component';

describe('AssessmentUnitTestComponent', () => {
  let component: AssessmentUnitTestComponent;
  let fixture: ComponentFixture<AssessmentUnitTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssessmentUnitTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssessmentUnitTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
