import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackJackPortalComponent } from './black-jack-portal.component';

describe('BlackJackPortalComponent', () => {
  let component: BlackJackPortalComponent;
  let fixture: ComponentFixture<BlackJackPortalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackJackPortalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackJackPortalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
