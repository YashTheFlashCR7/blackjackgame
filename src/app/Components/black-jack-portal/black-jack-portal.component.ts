import { Component, OnInit } from '@angular/core';
import { PlayerService } from 'src/app/Services/Player/player.service'
import { CardService } from 'src/app/Services/Card/card.service'
import { CommonService} from 'src/app/Services/Common/common.service'
import { FormGroup,FormControl, Validators} from '@angular/forms';
import { Player } from '../../Models/Player.model';

@Component({
  selector: 'app-black-jack-portal',
  templateUrl: './black-jack-portal.component.html',
  styleUrls: ['./black-jack-portal.component.css']
})
export class BlackJackPortalComponent implements OnInit {
  Variable:any
  PlayerArray=[]
  PlayerObject:any
  PlayerForm:FormGroup
  PlayerID:number = 1
  showPlayerCaptureForm:boolean
  DeckOfCards=[]
  firstHand;
  gameObject
  DealerObject=[]
  showStartButton:boolean
  showWelcomeForm:boolean
  DealerTurnActive:boolean
  FinalScoreObject=[]
  constructor(private PlayerService:PlayerService , private CardService:CardService , private CommonService:CommonService) { }

  ngOnInit() {
    this.PlayerObject=this.PlayerService.createPlayersObject()
    this.PlayerForm=this.PlayerService.createPlayerFormGroup()
    this.showPlayerCaptureForm=true
    this.DeckOfCards=this.CardService.createDeck()
    this.DeckOfCards=this.CardService.shuffle(this.DeckOfCards)
    this.showStartButton= false
    this.showWelcomeForm=true
    this.DealerTurnActive=false
  }

  onsubmit() {
    this.showWelcomeForm = false
    if (this.PlayerID < 5) {
        let PlayerObject = new Player(this.PlayerID, this.PlayerForm.controls['name'].value, false)
        this.PlayerArray.push(PlayerObject)
        if (this.PlayerID == 4) {
            this.showPlayerCaptureForm = false
            this.showStartButton = true
            this.assignPlayers(this.PlayerArray)
        } else {
            this.PlayerID = this.PlayerID + 1
        }

    }
}

assignPlayers(player) {
    let dealtCards: any;
    this.gameObject = this.PlayerService.createGameObject()
    for (var i = 0; i < player['length']; i++) {
        dealtCards = this.CardService.DealCards(this.DeckOfCards)
        this.gameObject.push({
            id: player[i]['id'],
            name: player[i]['name'],
            cards: dealtCards,
            score: "",
            status: "C",
            Position: 'N/A'
        })
    }
    this.gameObject.shift()
    this.calculatePlayerScores()
    this.CreateDealer()
}

CreateDealer() {
    let dealtCards = this.CardService.DealCards(this.DeckOfCards)
    this.DealerObject.push({
        id: 1,
        name: 'Dealer',
        cards: dealtCards,
        score: '',
        status: "C",
        Position: ''
    })
    this.calculateDealerScore(this.DealerObject[0]['id'])
}

Stand(PlayerID) {
    if (this.gameObject[PlayerID - 1]['score']['Value'] <= 21 || this.gameObject[PlayerID - 1]['score']['SecondaryValue'] <= 21) {
        this.gameObject[PlayerID - 1]['Position'] = "Stand"
    }

}

getCard(cards, id: number) {
    id = id - 1
    if (this.gameObject[id]['score']['Value'] < 21 || this.gameObject[id]['score']['SecondaryValue'] < 21) {
        if (this.gameObject[id]['Position'] != "Stand") {
            this.CardService.hitcard(this.DeckOfCards, cards, id)
            this.gameObject[id]['score'] = this.CommonService.calculateCardValue(this.gameObject[id].cards)
            this.gameObject[id]['status'] = this.CommonService.checkIfBust(this.gameObject[id]['score'])
        }
    }

}

getDealerCard(Dealer: any[], id: number) {
    id = id - 1
    if (this.CommonService.checkPlayerStatus(this.gameObject)) {
        this.DealerTurnActive = true
        if (this.gameObject[id]['score']['Value'] < 21 || this.gameObject[id]['score']['SecondaryValue'] < 21) {
            this.CardService.hitcard(this.DeckOfCards, this.DealerObject[id], id)
            this.DealerObject[id]['score'] = this.CommonService.calculateCardValue(this.DealerObject[id]['cards'])
            this.DealerObject[id]['status'] = this.CommonService.checkIfBust(this.DealerObject[id]['score'])
        }
    }
}

CheckEndGameScenarios() {
    for (var i = 1; i < this.gameObject['length'] + 1; i++) {
        this.FinalScoreObject.push(this.CommonService.CheckPlayerWinStatus(this.gameObject, i, this.DealerObject))
    }
}
calculatePlayerScores() {

    for (var j = 0; j < this.gameObject['length']; j++) {
        this.gameObject[j]['score'] = this.CommonService.calculateCardValue(this.gameObject[j]['cards'])
    }

}

calculateDealerScore(id) {
    id = id - 1
    this.DealerObject[id]['score'] = this.CommonService.calculateCardValue(this.DealerObject[id]['cards'])
}

}
