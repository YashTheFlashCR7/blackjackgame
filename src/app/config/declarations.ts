import { APP_INITIALIZER } from '@angular/core';
//components
import { AssessmentUnitTestComponent } from "src/app/Components/assessment-unit-test/assessment-unit-test.component";
import { BlackJackPortalComponent } from "src/app/Components/black-jack-portal/black-jack-portal.component";
//services
import { CardService } from "src/app/Services/Card/card.service";
import { CommonService } from "src/app/Services/Common/common.service";
import { PlayerService } from "src/app/Services/Player/player.service";


//CORE_REFERENCE_IMPORTS
/**
*declarations for @NgModule
*/
export const appProviders = [
  PlayerService,
  CommonService,
  PlayerService,
  ];


export const appDeclarations = [
BlackJackPortalComponent,
AssessmentUnitTestComponent
];

export const appRoutes = [{path: 'BlackJack', component: BlackJackPortalComponent},{path: 'UnitTest', component: AssessmentUnitTestComponent},{path: '', redirectTo: 'BlackJack', pathMatch: 'full'},]

