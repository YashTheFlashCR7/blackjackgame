import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { appRoutes } from './declarations';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { environment } from '../../environments/environment'
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import 'hammerjs';


import {
  MatMenuModule, MatDialogModule, MatTabsModule, MatInputModule,
  MatSnackBarModule, MatTooltipModule, MatIconModule, MatToolbarModule, MatListModule,
  MatCardModule, MatSidenavModule, MatButtonModule, MatSelectModule, MatSlideToggleModule,
  MatButtonToggleModule, MatFormFieldModule, MatCheckboxModule, MatAutocompleteModule, MatDatepickerModule,
  MatRadioModule, MatSliderModule, MatStepperModule, MatExpansionModule, MatChipsModule, MatProgressSpinnerModule,
  MatProgressBarModule, MatTableModule, MatSortModule, MatPaginatorModule, MatGridListModule, MatNativeDateModule
} from '@angular/material';

/**
*imports for @NgModule
*/
export const appImportModules: any = [
  BrowserModule,
  FormsModule,
  ReactiveFormsModule,
  BrowserAnimationsModule,
  RouterModule.forRoot(appRoutes),
  HttpModule,
  HttpClientModule,
  BsDropdownModule,
  TooltipModule,
  ModalModule,
  /**
   * Angular material components
   */
  MatMenuModule,
  MatDialogModule,
  MatSnackBarModule,
  MatTooltipModule,
  MatIconModule,
  MatToolbarModule,
  MatCardModule,
  MatSidenavModule,
  MatTabsModule,
  MatInputModule,
  MatButtonModule,
  MatListModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatButtonToggleModule,
  MatFormFieldModule,
  MatCheckboxModule,
  MatAutocompleteModule,
  MatDatepickerModule,
  MatRadioModule,
  MatSliderModule,
  MatStepperModule,
  MatExpansionModule,
  MatChipsModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatGridListModule,
  MatNativeDateModule,
];
