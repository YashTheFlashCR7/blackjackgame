import { Injectable } from '@angular/core';
import { Card } from "src/app/Models/Card.model";

@Injectable({
  providedIn: 'root'
})
export class CardService {
private Suit=[
{Value:'Hearts'},
{Value:'Spades'},
{Value:'Clubs'},
{Value:'Diamonds'}
]

private Rank=[
  {Rank:'Ace',Value:1 ,Secondary:11},
  {Rank:'Deuce',Value:2 ,Secondary:2},
  {Rank:'Three',Value:3 ,Secondary:3},
  {Rank:'Four',Value:4 ,Secondary:4},
  {Rank:'Five',Value:5 ,Secondary:5},
  {Rank:'Six',Value:6 ,Secondary:6},
  {Rank:'Seven',Value:7 ,Secondary:7},
  {Rank:'Eight',Value:8 ,Secondary:8},
  {Rank:'Nine',Value:9 ,Secondary:9},
  {Rank:'Ten',Value:10 ,Secondary:10},
  {Rank:'Jack',Value:10 ,Secondary:10},
  {Rank:'Queen',Value:10 ,Secondary:10},
  {Rank:'King',Value:10 ,Secondary:10}
]
  constructor() {
    
   }

   public createDeck(){
    
     let CardArray =[]
     for(var i =0;i<this.Suit['length'];i++){
       for(var j=0;j<this.Rank['length'];j++){
         //assign suit and then iterate through ranks 
        let  cardModel = new Card(this.Suit[i]['Value'],this.Rank[j]['Rank'],this.Rank[j]['Value'],this.Rank[j]['Secondary'])
         CardArray.push(cardModel)
       }
     }

     return CardArray;
   }

   public hitcard(array:any[] , Player:any[] ,id){
    console.log(Player)
    let TempDealtCard:any = {}
    // get card from the top of the deck
    TempDealtCard = array.shift();
     Player['cards'].push(TempDealtCard)
   }

   public DealCards(array:any[]){
      let returnCards:any= []
      let TempDealtCard:any = {}
      for(var i=0;i<2;i++){
      // get card from the top of the deck
      TempDealtCard = array.shift();
      // push it to return array
      returnCards.push(TempDealtCard)
      }
      
      return returnCards
   }

   public shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  
    return array;
  }

}
