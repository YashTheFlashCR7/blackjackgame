import { Injectable } from '@angular/core';
import { Player } from "src/app/Models/Player.model";
import { FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private FormBuilder:FormBuilder) { }

  createPlayersObject() {
    let PlayersArray = new Player('', '', false)
    return PlayersArray
}

//create form group for input field and add validation
createPlayerFormGroup() {
    var PlayerForm = this.FormBuilder.group({
        name: ['', [Validators.required, Validators.pattern("[a-zA-Z ]*")]],
    })
    return PlayerForm;
}

//create in game object array
createGameObject() {
    var obj = [{
        id: '',
        name: '',
        cards: [{
            Suite: '',
            Rank: '',
            Value: '',
            Secondary: ''
        }],
        score: '',
    }]
    return obj
}
}
