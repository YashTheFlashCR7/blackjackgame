import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor() { }

  calculateCardValue(cardArray: any[]) {
    var ValueObj = {
        Value: 0,
        SecondaryValue: 0
    }
    for (var i = 0; i < cardArray['length']; i++) {
        ValueObj['Value'] = ValueObj['Value'] + cardArray[i]['Value']
        ValueObj['SecondaryValue'] = ValueObj['SecondaryValue'] + cardArray[i]['Secondary']
    }
    return ValueObj
}


checkIfBust(CardValueObj: any) {
    if (CardValueObj['Value'] === 21 || CardValueObj['SecondaryValue'] === 21) {
        return "BlackJack achieved"
    } else if (CardValueObj['Value'] > 21 && CardValueObj['SecondaryValue'] > 21) {
        return "Total Bust"
    } else {
        return "C"
    }
}

checkPlayerStatus(PlayerObject: any[]) {
    let Flag: boolean = true
    //check if all players have completed there turns
    for (var i = 0; i < PlayerObject['length']; i++) {
        if (PlayerObject[i]['Position'] != "Stand") {
            if (PlayerObject[i]['status'] === "C" || PlayerObject[i]['status'] === "") {
                Flag = false
                i = PlayerObject['length']
            }
        }

    }
    return Flag
}

CheckPlayerWinStatus(PlayerObject: any[], PlayerID, DealerObject: any[]) {
    let DealerIndex = 0
    let PlayerIndex = PlayerID - 1
    let StatusDescription: string
    let StatusObect = {}
    //check if Dealer is bust
    if (DealerObject[DealerIndex]['status'] != "Total Bust") {
        if (PlayerObject[PlayerIndex]['status'] != "Total Bust") {
            if (PlayerObject[PlayerIndex]['status'] === "BlackJack achieved" && DealerObject[DealerIndex]['status'] !== "BlackJack achieved") {
                StatusDescription = 'Beats dealer' //Player holds blackjack and dealer doesnt
            } else if (PlayerObject[PlayerIndex]['status'] != "BlackJack achieved" && DealerObject[DealerIndex]['status'] === "BlackJack achieved") {
                StatusDescription = 'Loses' //Dealer holds blackjack and dealer doesnt 
            } 
             else if(PlayerObject[PlayerIndex]['cards']['length']===5){
                StatusDescription = 'Beats dealer' //Player holds 5 cards without exceeding 21
             }
            
            else {
                    //check preliminary value comparisons
                if (PlayerObject[PlayerIndex]['score']['Value'] < DealerObject[DealerIndex]['score']['Value'] &&
                    PlayerObject[PlayerIndex]['score']['Value'] < DealerObject[DealerIndex]['score']['SecondaryValue']) {
                    //check secondary value comparisons
                    if (PlayerObject[PlayerIndex]['score']['SecondaryValue'] < DealerObject[DealerIndex]['score']['Value'] &&
                        PlayerObject[PlayerIndex]['score']['SecondaryValue'] < DealerObject[DealerIndex]['score']['SecondaryValue']) {
                        StatusDescription = 'loses'
                    } else {
                        StatusDescription = 'Beats dealer'
                    }
                } else {
                    StatusDescription = 'Beats dealer'
                }
            }
        } else {
            StatusDescription = 'Loses'
        }

    } else if (PlayerObject[PlayerIndex]['status'] != "Total Bust") {
        StatusDescription = 'Beats dealer' //player is not bust and dealer is 
    } else {
        StatusDescription = 'Loses' //player is bust
    }


    StatusObect = {
        name: PlayerObject[PlayerIndex]['name'],
        gamestatus: StatusDescription
    }
    return StatusObect
  }
}

