import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { appImportModules } from "./config/import-modules";
import { appDeclarations,appProviders } from "./config/declarations";



@NgModule({
  declarations: [AppComponent,...appDeclarations,],
  imports: [...appImportModules],
  providers: [...appProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
